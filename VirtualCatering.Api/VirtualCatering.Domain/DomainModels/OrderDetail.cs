﻿namespace VirtualCatering.Domain.DomainModels
{
    /// <summary>
    /// Klasa reprezentująca model detali zamówienia.
    /// </summary>
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Units { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
