﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace VirtualCatering.Domain.DomainModels
{
    /// <summary>
    /// Klasa reprezentująca model użytkowika.
    /// </summary>
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Role { get; set; }
        public string Login { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Order> WaiterOrders { get; set; }
        public virtual ICollection<Order> TableOrders { get; set; }
        public virtual ICollection<Order> KitchenOrders { get; set; }
        public virtual ICollection<Notification> WaiterNotifications { get; set; }
    }
}
