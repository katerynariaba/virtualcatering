﻿using System.ComponentModel.DataAnnotations;

namespace VirtualCatering.Domain.DomainModels
{
    /// <summary>
    /// Klasa reprezentująca model produktu.
    /// </summary>
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public float Weight { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
    }
}
