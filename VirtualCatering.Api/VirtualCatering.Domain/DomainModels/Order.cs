﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VirtualCatering.Domain.DomainModels
{
    /// <summary>
    /// Klasa reprezentująca model zamówienia.
    /// </summary>
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int TableId { get; set; }
        public int? WaiterId { get; set; }
        public int? KitchenId { get; set; }
        public int NumberOfClients { get; set; }
        [DefaultValue(false)]
        public bool IsClosed { get; set; }
        public DateTime DateTime { get; set; }
        public string Comment { get; set; }

        public virtual User Table { get; set; }
        public virtual User Waiter { get; set; }
        public virtual User Kitchen { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }


    }
}
