﻿using System.ComponentModel.DataAnnotations;

namespace VirtualCatering.Domain.DomainModels
{
    /// <summary>
    /// Klasa reprezentująca model notyfikacji.
    /// </summary>
    public class Notification
    {
        [Key]
        public int Id { get; set; }
        public int TableId { get; set; }
        public int WaiterId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public virtual User Table { get; set; }
        public virtual User Waiter { get; set; }
    }
}
