﻿using Microsoft.EntityFrameworkCore;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Domain.Db
{
    /// <summary>
    /// Klasa, która reprezentuje DbContext systemu.
    /// </summary>
    public class VirtualCateringDbContext : DbContext
    {
        public VirtualCateringDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Metoda, do konfiguracji modeli.
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                        .HasOne(m => m.Waiter)
                        .WithMany(t => t.WaiterOrders)
                        .HasForeignKey(m => m.WaiterId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
                        .HasOne(m => m.Table)
                        .WithMany(t => t.TableOrders)
                        .HasForeignKey(m => m.TableId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
                        .HasOne(m => m.Kitchen)
                        .WithMany(t => t.KitchenOrders)
                        .HasForeignKey(m => m.KitchenId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Notification>()
                        .HasOne(m => m.Waiter)
                        .WithMany(t => t.WaiterNotifications)
                        .HasForeignKey(m => m.WaiterId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OrderDetail>()
                .HasOne(e => e.Order)
                .WithMany(c => c.OrderDetails)
                .HasForeignKey(m => m.OrderId)
                .OnDelete(DeleteBehavior.Restrict);

            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Notification> Notifications { get; set; }
    }
}
