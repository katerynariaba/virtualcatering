﻿using FluentValidation;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji produktu.
    /// </summary>
    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(product => product.Title).MinimumLength(4);
            RuleFor(product => product.Price).GreaterThan(0.0).NotEmpty();
            RuleFor(product => product.Weight).GreaterThan(0.0f).NotEmpty();
            RuleFor(product => product.Type).NotEmpty();
        }
    }
}
