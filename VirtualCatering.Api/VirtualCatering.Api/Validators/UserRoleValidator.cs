﻿using FluentValidation.Validators;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji roli usera.
    /// </summary>
    public class UserRoleValidator : PropertyValidator
    {
        public UserRoleValidator() : base("User role {PropertyValue} is not a valid type")
        {
        }
        protected override bool IsValid(PropertyValidatorContext context)
        {
            string userRole = (string)context.PropertyValue;
            if (userRole.ToLower() == "waiter" || userRole.ToLower() == "table" || userRole.ToLower() == "kitchen")
                return true;

            return false;
        }
    }
}
