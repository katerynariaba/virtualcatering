﻿using FluentValidation;
using VirtualCatering.Api.DtoModels;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji detali zamówienia.
    /// </summary>
    public class OrderDetailsValidator : AbstractValidator<OrderDetailDto>
    {
        public OrderDetailsValidator()
        {
            RuleFor(od => od.OrderId).NotEmpty();
            RuleFor(detail => detail.ProductId).NotEmpty();
            RuleFor(detail => detail.Units).NotEmpty().GreaterThan(0);
        }
    }
}
