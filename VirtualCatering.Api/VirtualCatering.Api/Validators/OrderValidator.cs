﻿using FluentValidation;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji zamówienia.
    /// </summary>
    public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(order => order.TableId).NotEmpty();
            RuleFor(order => order.NumberOfClients).GreaterThan(0);
        }
    }
}
