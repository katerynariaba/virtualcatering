﻿using FluentValidation;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do dodani funkcji walidacji roli usera.
    /// </summary>
    public static class UserValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> ValidUserRole<T>(
        this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new UserRoleValidator());
        }
    }
}
