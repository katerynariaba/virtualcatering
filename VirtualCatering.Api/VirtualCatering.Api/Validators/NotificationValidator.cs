﻿using FluentValidation;
using VirtualCatering.Api.DtoModels;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji notyfikacji.
    /// </summary>
    public class NotificationValidator : AbstractValidator<NotificationDto>
    {
        public NotificationValidator()
        {
            RuleFor(n => n.Title).NotEmpty();
        }
    }
}
