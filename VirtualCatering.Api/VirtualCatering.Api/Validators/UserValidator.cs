﻿using FluentValidation;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Validators
{
    /// <summary>
    /// Klasa do walidacji usera.
    /// </summary>
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.Role).NotEmpty().ValidUserRole();
            RuleFor(user => user.Login).Length(4, 15);
            RuleFor(user => user.Description).MaximumLength(50);
        }
    }
}
