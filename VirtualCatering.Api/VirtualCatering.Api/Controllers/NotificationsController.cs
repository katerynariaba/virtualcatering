﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Hubs;
using VirtualCatering.Api.Models;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do endpointów związanych z notyfikacjami.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationsController : ControllerBase
    {
        private VirtualCateringDbContext _context;
        private readonly IMapper _mapper;
        IHubContext<NotificationHub> _hubContext;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public NotificationsController(VirtualCateringDbContext context, IMapper mapper, IHubContext<NotificationHub> hubContext)
        {
            _context = context;
            _mapper = mapper;
            _hubContext = hubContext;
        }

        /// <summary>
        /// Zwraca id zalogowanego usera.
        /// </summary>
        private int GetUserId()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var sub = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;

            try
            {
                return Int32.Parse(sub);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        /// <summary>
        /// Zwraca listę wszystkich notyfikacji zapisanych w bazie danych według waiterId.
        /// </summary>
        /// <param name="id">Int reprezentujący id książki</param>
        [HttpGet]
        [Authorize(Roles = Role.waiter)]
        public IActionResult GetByWaiterId()
        {
            var waiterId = GetUserId();
            var notifications = _context.Notifications.Where(r => r.WaiterId == waiterId).ToList();
            var notificationsDto = _mapper.Map<List<NotificationDto>>(notifications);

            return Ok(notificationsDto);
        }

        /// <summary>
        /// Zwraca ról zalogowanego usera.
        /// </summary>
        private string GetUserRole()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var sub = claimsIdentity.FindFirst(ClaimTypes.Role)?.Value;

            return sub;
        }

        /// <summary>
        /// Dodaje notyfikacji do bazy danych.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = Role.table)]
        public async Task<IActionResult> Add(NotificationDto notificationDbo)
        {
            var userId = GetUserId();
            var role = GetUserRole();
            int? orderWaiterId;

            if (role == Role.table)
            {
                notificationDbo.TableId = userId;
                orderWaiterId = _context.Orders
                    .Where(r => r.TableId == userId && r.IsClosed == false)
                    .Select(order => order.WaiterId)
                    .FirstOrDefault();

                var tableDescription = await _context.Users
                    .Where(user => user.Id == userId)
                    .Select(user => user.Description)
                    .FirstOrDefaultAsync();

                notificationDbo.Title = tableDescription + notificationDbo.Title;
            }
            else
            {
                orderWaiterId = notificationDbo.WaiterId;
            }

            var randomWaiterId = await _context.Users
                .Where(user => user.Role == Role.waiter)
                .Select(user => user.Id)
                .FirstOrDefaultAsync();

            var waiterId = orderWaiterId ?? randomWaiterId;

            notificationDbo.WaiterId = waiterId;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (_context.Orders.Any(r => r.Id == notificationDbo.Id))
            {
                throw new Exception($"Notification with ID:{notificationDbo.Id} already exists");
            }

            var notification = _mapper.Map<Notification>(notificationDbo);

            await _context.Notifications.AddAsync(notification);
            await _context.SaveChangesAsync();

            await _hubContext.Clients.All.SendAsync("NewNotificationMessage", notification);

            return Ok();
        }

        /// <summary>
        /// Usuwa notyfikacji z bazy danych.
        /// </summary>
        /// <param name="id">Id notyfikacji, którą usuwamy.</param>
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = Role.waiter)]
        public IActionResult Delete(int id)
        {
            Notification notification = _context.Notifications.FirstOrDefault(r => r.Id == id);
            _context.Notifications.Remove(notification);
            _context.SaveChanges();
            return Ok();
        }
    }
}