﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VirtualCatering.Api.Models;
using VirtualCatering.Api.Services.Abstract;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do autetnyfikacji oraz autoryzacji.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private IAuthService _authServ;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public AuthController(IAuthService authServ)
        {
            _authServ = authServ;
        }

        /// <summary>
        /// Endpoint do autentyfikacji użytkownika.
        /// </summary>
        /// <param name="user">User, którego autentyfikujemy</param>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody]AuthModel model)
        {
            var user = _authServ.Authenticate(model.Login, model.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(user);
        }
    }
}
