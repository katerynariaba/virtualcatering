﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Models;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do endpointów związanych z produktami.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Zwraca listę wszystkich użytkowników zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id użytkownika</param>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);
            var userDto = _mapper.Map<UserDto>(user);

            return Ok(userDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich użytkowników zapisanych w bazie danych.
        /// </summary>
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var usersDto = _mapper.Map<List<UserDto>>(users);

            return Ok(usersDto);
        }
    }
}