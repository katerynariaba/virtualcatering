﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Models;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do endpointów związanych z detali baz danych.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class OrderDetailsController : ControllerBase
    {
        private IOrderDetailsService _odService;
        private readonly IMapper _mapper;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public OrderDetailsController(IOrderDetailsService odService, IMapper mapper)
        {
            _odService = odService;
            _mapper = mapper;
        }

        /// <summary>
        /// Dodaje detali zamówienia do bazy danych.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = Role.table + "," + Role.waiter)]
        public IActionResult Add(OrderDetailDto odDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var od = _mapper.Map<OrderDetail>(odDto);

            _odService.Add(od);
            return Ok();
        }
    }
}