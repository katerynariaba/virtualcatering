﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Models;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do endpointów związanych z produktami.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public ProductsController(IMapper mapper, IProductService productService)
        {
            _mapper = mapper;
            _productService = productService;
        }

        /// <summary>
        /// Zwraca listę wszystkich produktów zapisanych w bazie danych.
        /// </summary>
        [HttpGet]
        public IActionResult GetAll()
        {
            var products = _productService.GetAll();
            var productsDto = _mapper.Map<List<ProductDto>>(products);

            return Ok(productsDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich produktów zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id produktów</param>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var product = _productService.GetById(id);
            var productDto = _mapper.Map<ProductDto>(product);

            return Ok(productDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich produktó zapisanych w bazie danych wegług podanego typu
        /// </summary>
        /// <param name="type">String reprezentujący type produktu</param>
        [HttpGet]
        [Route("type={type}")]
        public IActionResult GetByType(string type)
        {
            var products = _productService.GetByType(type);
            var productsDto = _mapper.Map<List<ProductDto>>(products);

            return Ok(productsDto);
        }

        /// <summary>
        /// Dodaje produkt do bazy danych.
        /// </summary>
        /// /// <param name="productDto">Produkt, który dodajemy</param>
        [HttpPost]
        [Authorize(Roles = Role.kitchen)]
        public IActionResult Add(ProductDto productDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (_productService.GetById(productDto.Id) != null)
            {
                throw new Exception($"Product with ID:{productDto.Id} already exists");
            }

            var product = _mapper.Map<Product>(productDto);

            _productService.Add(product);
            return Ok();
        }

        /// <summary>
        /// Usuwa produkt z bazy danych.
        /// </summary>
        /// <param name="id">Id produktu, który usuwamy.</param>
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = Role.kitchen)]
        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return Ok();
        }
    }
}