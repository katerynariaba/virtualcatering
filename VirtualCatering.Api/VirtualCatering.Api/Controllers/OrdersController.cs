﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Hubs;
using VirtualCatering.Api.Models;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Controllers
{
    /// <summary>
    /// Kontroler do endpointów związanych z zamówieniami.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private VirtualCateringDbContext _context;
        private IOrderService _orderService;
        private readonly IMapper _mapper;
        IHubContext<OrderHub> _hubContext;

        /// <summary>
        ///Konstruktor kontrolera
        /// </summary>
        public OrdersController(VirtualCateringDbContext context, IMapper mapper, IHubContext<OrderHub> hubContext, IOrderService orderService)
        {
            _context = context;
            _mapper = mapper;
            _hubContext = hubContext;
            _orderService = orderService;
        }

        /// <summary>
        /// Zwraca id zalogowanego usera.
        /// </summary>
        private int GetUserId()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var sub = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;

            try
            {
                return Int32.Parse(sub);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        /// <summary>
        /// Zwraca ról zalogowanego usera.
        /// </summary>
        private string GetUserRole()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var sub = claimsIdentity.FindFirst(ClaimTypes.Role)?.Value;

            return sub;
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych bez kelnera.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = Role.waiter)]
        public IActionResult GetAllWithoutWaiter()
        {
            var orders = _orderService.GetAllWithoutWaiter();
            var ordersDto = _mapper.Map<List<OrderDto>>(orders);
            return Ok(ordersDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych z kelnerem dodanym.
        /// </summary>
        [HttpGet]
        [Route("all")]
        [Authorize(Roles = Role.kitchen)]
        public IActionResult GetAllWithWaiter()
        {
            var orders = _orderService.GetAllWithWaiter();
            var ordersDto = _mapper.Map<List<OrderDto>>(orders);

            return Ok(ordersDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych za id zalogowanego kelnera.
        /// </summary>
        [HttpGet]
        [Route("all-by-waiter")]
        [Authorize(Roles = Role.waiter)]
        public IActionResult GetByWaiterId()
        {
            var waiterid = GetUserId();

            var orders = _orderService.GetByWaiterId(waiterid);
            var ordersDto = _mapper.Map<List<OrderDto>>(orders);

            return Ok(ordersDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówien zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id zamówienia</param>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var order = _orderService.GetById(id);

            var orderDto = _mapper.Map<OrderDto>(order);

            //orderDto.TotalPrice = order.OrderDetails.Select(r => r.Product.Price * r.Units).Sum();
            orderDto.TableDescription = _context.Users.FirstOrDefault(r => r.Id == orderDto.TableId).Description;

            return Ok(orderDto);
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówien zapisanych w bazie danych wegług podanego id zalogowanego stolika.
        /// </summary>
        [HttpGet]
        [Route("my-last")]
        public IActionResult GetByTableId()
        {
            var id = GetUserId();

            var orders = _orderService.GetByTableId(id);

            var ordersDto = _mapper.Map<List<OrderDto>>(orders);

            return Ok(ordersDto);
        }

        /// <summary>
        /// Dodaje zamówienie do bazy danych.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = Role.table + "," + Role.waiter)]
        public async Task<IActionResult> Add(OrderDto orderDto)
        {
            var role = GetUserRole();

            if (role == Role.table)
            {
                orderDto.TableId = GetUserId();
            }
            else
            {
                orderDto.WaiterId = GetUserId();
            }

            orderDto.DateTime = DateTime.Now;

            var tableOrder = _context.Orders
                .FirstOrDefault(r => r.TableId == orderDto.TableId && r.IsClosed == false);

            if (tableOrder != null)
            {
                orderDto.WaiterId = tableOrder.WaiterId;
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (_context.Orders.Any(r => r.Id == orderDto.Id))
            {
                throw new Exception($"Order with ID:{orderDto.Id} already exists");
            }
            var order = _mapper.Map<Order>(orderDto);
            var orderDetails = orderDto.OrderDetails.ToList();

            await _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();

            foreach (OrderDetail od in orderDetails)
            {
                od.OrderId = order.Id;
                await _context.OrderDetails.AddAsync(od);
            }

            await _context.SaveChangesAsync();

            await _hubContext.Clients.All.SendAsync("NewOrderMessage", order);

            return Ok();
        }

        /// <summary>
        /// Modyfikuje dane zamówienia - dodaje do zamówienia id zalogowanego kelnera.
        /// </summary>
        /// <param name="orderId">Id zamówienia, dane którego modyfikujemy.</param>
        [HttpPut]
        [Route("{orderId}")]
        [Authorize(Roles = Role.waiter)]
        public IActionResult AssignToWaiter(int orderId)
        {
            var waiterId = GetUserId();

            var currentOrder = _context.Orders.FirstOrDefault(r => r.Id == orderId);

            var allOrders = _context.Orders.Where(r => r.TableId == currentOrder.TableId && r.IsClosed == false).ToList();

            foreach (Order order in allOrders)
            {
                order.WaiterId = waiterId;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            return Ok();
        }

        /// <summary>
        /// Modyfikuje dane zamówienia - zamyka zamówienie.
        /// </summary>
        /// <param name="orderId">Id zamówienia, dane którego modyfikujemy.</param>
        [HttpPut]
        [Route("{orderId}/close")]
        [Authorize(Roles = Role.waiter)]
        public IActionResult Close(int orderId)
        {
            var order = _context.Orders.FirstOrDefault(r => r.Id == orderId);
            order.IsClosed = true;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _context.Orders.Update(order);
            _context.SaveChanges();
            return Ok();
        }
    }
}
