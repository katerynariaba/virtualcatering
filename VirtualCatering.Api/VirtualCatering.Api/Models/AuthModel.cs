﻿using System.ComponentModel.DataAnnotations;

namespace VirtualCatering.Api.Models
{
    /// <summary>
    /// Klasa, reprezentująca dane niezbędne do autoryzacji.
    /// </summary>
    public class AuthModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
