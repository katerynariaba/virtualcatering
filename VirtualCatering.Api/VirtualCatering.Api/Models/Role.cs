﻿namespace VirtualCatering.Api.Models
{
    /// <summary>
    /// Klasa, reprezentująca dane do roli.
    /// </summary>
    public static class Role
    {
        public const string waiter = "waiter";
        public const string table = "table";
        public const string kitchen = "kitchen";
    }
}
