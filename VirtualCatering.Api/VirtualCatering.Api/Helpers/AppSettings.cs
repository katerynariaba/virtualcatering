﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCatering.Api.Helpers
{
    /// <summary>
    /// Helper do JWT.
    /// </summary>
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
