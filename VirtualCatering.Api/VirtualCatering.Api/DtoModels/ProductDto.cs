﻿namespace VirtualCatering.Api.DtoModels
{
    /// <summary>
    /// Klasa, reprezentująca DTO produktu.
    /// </summary>
    public class ProductDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public float Weight { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
    }
}
