﻿using System.Collections.Generic;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.DtoModels
{
    /// <summary>
    /// Klasa, reprezentująca DTO użytkownika.
    /// </summary>
    public class UserDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public string Token { get; set; }
    }
}
