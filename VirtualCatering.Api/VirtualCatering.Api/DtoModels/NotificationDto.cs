﻿namespace VirtualCatering.Api.DtoModels
{
    /// <summary>
    /// Klasa, reprezentująca DTO notyfikacji.
    /// </summary>
    public class NotificationDto
    {
        public int Id { get; set; }
        public int TableId { get; set; }
        public int WaiterId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
