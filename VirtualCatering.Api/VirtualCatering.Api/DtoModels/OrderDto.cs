﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.DtoModels
{
    /// <summary>
    /// Klasa, reprezentująca DTO zamówienia.
    /// </summary>
    public class OrderDto
    {
        public int Id { get; set; }
        public int TableId { get; set; }
        public int? WaiterId { get; set; }
        public int? KitchenId { get; set; }
        public int NumberOfClients { get; set; }
        public double TotalPrice { get; set; }
        public bool IsClosed { get; set; }
        public DateTime DateTime { get; set; }
        public string TableDescription { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
