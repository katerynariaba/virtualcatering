﻿namespace VirtualCatering.Api.DtoModels
{
    /// <summary>
    /// Klasa, reprezentująca DTO detali zamówienia.
    /// </summary>
    public class OrderDetailDto
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Units { get; set; }
    }
}
