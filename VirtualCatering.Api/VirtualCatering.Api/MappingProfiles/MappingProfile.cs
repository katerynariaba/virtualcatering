﻿using AutoMapper;
using System.Linq;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.MappingProfiles
{
    /// <summary>
    /// Klasa do mapowania modeli.
    /// </summary>
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<Order, OrderDto>()
                .ForMember(
                    distination => distination.TotalPrice,
                    opt => opt.MapFrom(src => src.OrderDetails.Aggregate(0.0, (acc, detail) => acc + detail.Units * detail.Product.Price))
                );
            CreateMap<OrderDetail, OrderDetailDto>();
            CreateMap<Product, ProductDto>();
            CreateMap<Notification, NotificationDto>();

            CreateMap<NotificationDto, Notification>();
            CreateMap<OrderDetailDto, OrderDetail>();
            CreateMap<ProductDto, Product>();
            CreateMap<OrderDto, Order>();
        }
    }
}
