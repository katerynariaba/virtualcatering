﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Concrete
{
    public class UserService : IUserService
    {
        private readonly VirtualCateringDbContext _context;

        public UserService(VirtualCateringDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Zwraca listę wszystkich użytkowników zapisanych w bazie danych.
        /// </summary>
        public List<User> GetAll()
        {
            return _context.Users.ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich użytkowników zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id użytkownika</param>
        public User GetById(int id)
        {
            return _context.Users.FirstOrDefault(r => r.Id == id);
        }
    }
}
