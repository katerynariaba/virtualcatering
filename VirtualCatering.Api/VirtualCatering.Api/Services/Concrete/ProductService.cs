﻿using System.Collections.Generic;
using System.Linq;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Concrete
{
    public class ProductService : IProductService
    {
        private readonly VirtualCateringDbContext _context;

        public ProductService(VirtualCateringDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Zwraca listę wszystkich produktów zapisanych w bazie danych.
        /// </summary>
        public List<Product> GetAll()
        {
            return _context.Products.ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich produktów zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id produktów</param>
        public Product GetById(int id)
        {
            return _context.Products.FirstOrDefault(r => r.Id == id);
        }

        /// <summary>
        /// Zwraca listę wszystkich produktów zapisanych w bazie danych wegług podanego typu
        /// </summary>
        /// <param name="type">String reprezentujący type produktu</param>
        public List<Product> GetByType(string type)
        {
            return _context.Products.Where(r => r.Type == type).ToList();
        }

        /// <summary>
        /// Dodaje produkt do bazy danych.
        /// </summary>
        /// /// <param name="product">Produkt, który dodajemy</param>
        public void Add(Product product)
        {
            _context.Add(product);
            _context.SaveChanges();
        }

        /// <summary>
        /// Usuwa produkt z bazy danych.
        /// </summary>
        /// <param name="id">Id produktu, który usuwamy.</param>
        public void Delete(int id)
        {
            _context.Remove(GetById(id));
            _context.SaveChanges();
        }
    }
}
