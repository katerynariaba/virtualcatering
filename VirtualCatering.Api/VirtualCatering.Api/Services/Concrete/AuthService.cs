﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using VirtualCatering.Api.DtoModels;
using VirtualCatering.Api.Helpers;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;

namespace VirtualCatering.Api.Services.Concrete
{
    public class AuthService : IAuthService
    {
        private readonly AppSettings _appSettings;

        private VirtualCateringDbContext _context;
        private readonly IMapper _mapper;

        public AuthService(VirtualCateringDbContext context, IOptions<AppSettings> appSettings, IMapper mapper)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _mapper = mapper;
        }

        public UserDto Authenticate(string login, string password)
        {
            var user = _context.Users.SingleOrDefault(x => x.Login == login && x.Password == password);

            if (user == null)
                return null;

            var userDto = _mapper.Map<UserDto>(user);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userDto.Id.ToString()),
                    new Claim(ClaimTypes.Role, userDto.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            userDto.Token = tokenHandler.WriteToken(token);

            return userDto;
        }
    }
}
