﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Concrete
{
    public class OrderService : IOrderService
    {
        private readonly VirtualCateringDbContext _context;

        public OrderService(VirtualCateringDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych bez kelnera.
        /// </summary>
        public List<Order> GetAllWithoutWaiter()
        {
            return _context.Orders.Where(r => r.WaiterId == null).ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych z kelnerem dodanym.
        /// </summary>
        public List<Order> GetAllWithWaiter()
        {
            return _context.Orders.Where(r => r.WaiterId != null).ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówień zapisanych w bazie danych za id zalogowanego kelnera.
        /// </summary>
        public List<Order> GetByWaiterId(int waiterid)
        {
            return _context.Orders.Where(r => r.WaiterId == waiterid && r.IsClosed == false).ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówien zapisanych w bazie danych wegług podanego id zalogowanego stolika.
        /// </summary>
        public List<Order> GetByTableId(int tableid)
        {
            return _context.Orders
                .Include(order => order.OrderDetails)
                .ThenInclude(detail => detail.Product)
                .Where(r => r.TableId == tableid && r.IsClosed == false)
                .ToList();
        }

        /// <summary>
        /// Zwraca listę wszystkich zamówien zapisanych w bazie danych wegług podanego id.
        /// </summary>
        /// <param name="id">Int reprezentujący id zamówienia</param>
        public Order GetById(int id)
        {
            return _context.Orders
                .Include(order => order.OrderDetails)
                .ThenInclude(detail => detail.Product)
                .FirstOrDefault(r => r.Id == id);
        }

        /// <summary>
        /// Dodaje zamówienie do bazy danych.
        /// </summary>
        public void Add(Order order)
        {
            _context.Add(order);
            _context.SaveChanges();
        }
    }
}
