﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.Db;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Concrete
{
    public class OrderDetailService : IOrderDetailsService
    {
        private readonly VirtualCateringDbContext _context;

        public OrderDetailService(VirtualCateringDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Dodaje detali zamówienia do bazy danych.
        /// </summary>
        /// /// <param name="od">Detal, który dodajemy</param>
        public void Add(OrderDetail od)
        {
            _context.Add(od);
            _context.SaveChanges();
        }
    }
}
