﻿using VirtualCatering.Api.DtoModels;

namespace VirtualCatering.Api.Services.Abstract
{
    public interface IAuthService
    {
        UserDto Authenticate(string login, string password);
    }
}
