﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Abstract
{
    public interface IUserService
    {
        public List<User> GetAll();
        public User GetById(int id);
    }
}
