﻿using System.Collections.Generic;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Abstract
{
    public interface IProductService
    {
        public List<Product> GetAll();
        public Product GetById(int id);
        public List<Product> GetByType(string type);
        public void Add(Product product);
        public void Delete(int id);
    }
}
