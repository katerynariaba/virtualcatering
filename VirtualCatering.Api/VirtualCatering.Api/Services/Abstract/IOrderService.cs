﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Abstract
{
    public interface IOrderService
    {
        public List<Order> GetAllWithoutWaiter();
        public List<Order> GetAllWithWaiter();
        public Order GetById(int id);
        public List<Order> GetByWaiterId(int id);
        public List<Order> GetByTableId(int id);
        public void Add(Order order);
    }
}
