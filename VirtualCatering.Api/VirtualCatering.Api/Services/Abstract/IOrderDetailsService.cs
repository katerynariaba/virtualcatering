﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCatering.Domain.DomainModels;

namespace VirtualCatering.Api.Services.Abstract
{
    public interface IOrderDetailsService
    {
        public void Add(OrderDetail od);
    }
}
