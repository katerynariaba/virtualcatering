﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.DomainModels;
using Xunit;

namespace VirtualCatering.UnitTests
{
    public class OrderUnitTests
    {
        [Fact]
        public void IfGetAllWithWaiterReturnCorrectQuantity()
        {
            //Arrange
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetAllWithWaiter();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
        }

        [Fact]
        public void IfGetAllWithoutWaiterAreNotNull()
        {
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetAllWithWaiter();

            //Assert
            Assert.NotNull(actualResult);
        }

        [Fact]
        public void IfAllWithoutWaiterReturnCorrectQuantity()
        {
            //Arrange
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithoutWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetAllWithoutWaiter();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
        }

        [Fact]
        public void IfAllWithoutWaiterAreNotNull()
        {
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithoutWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetAllWithoutWaiter();

            //Assert
            Assert.NotNull(actualResult);
        }

        [Fact]
        public void IfGetByWaiterIdNotNull()
        {
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  WaiterId = 1,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetByWaiterId(1);

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public void IfGetByTableIdNotNull()
        {
            var mockOrderService = new Mock<IOrderService>();
            var expectedResult = new List<Order>()
              {
                new Order()
                {
                  Id = 0,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
                new Order()
                {
                  Id = 1,
                  TableId = 3,
                  NumberOfClients = 3,
                  IsClosed = false
                },
              };

            mockOrderService.Setup(r => r.GetAllWithoutWaiter()).Returns(expectedResult);

            //Act
            var actualResult = mockOrderService.Object.GetByTableId(3);

            //Assert
            Assert.Null(actualResult);
        }
    }
}
