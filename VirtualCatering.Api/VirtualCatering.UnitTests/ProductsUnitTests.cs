﻿using Moq;
using System.Collections.Generic;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.DomainModels;
using Xunit;

namespace VirtualCatering.UnitTests
{
    public class ProductsUnitTests
    {
        [Fact]
        public void IfGetAllReturnCorrectQuantity()
        {
            //Arrange
            var mockProductService = new Mock<IProductService>();
            var expectedResult = new List<Product>()
              {
                new Product()
                {
                  Id = 0,
                  Title = "title",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type",
                  Image = "image"
                },
                new Product()
                {
                  Id = 1,
                  Title = "title1",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type1",
                  Image = "image1"
                },
              };

            mockProductService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockProductService.Object.GetAll();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
        }

        [Fact]
        public void IfGetAllAllItemsAreNotNull()
        {
            //Arrange
            var mockProductService = new Mock<IProductService>();
            var expectedResult = new List<Product>()
              {
                new Product()
                {
                  Id = 0,
                  Title = "title",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type",
                  Image = "image"
                },
                new Product()
                {
                  Id = 1,
                  Title = "title1",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type1",
                  Image = "image1"
                },
              };

            mockProductService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockProductService.Object.GetAll();

            //Assert
            Assert.NotNull(actualResult);
        }

        [Fact]
        public void IfGetByIdReturnNotNull()
        {
            //Arrange
            var mockProductService = new Mock<IProductService>();
            var expectedResult = new List<Product>()
              {
                new Product()
                {
                  Id = 0,
                  Title = "title",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type",
                  Image = "image"
                },
                new Product()
                {
                  Id = 1,
                  Title = "title1",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type1",
                  Image = "image1"
                },
              };

            mockProductService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockProductService.Object.GetById(0);

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public void IfGetByTypeReturnNotNull()
        {
            //Arrange
            var mockProductService = new Mock<IProductService>();
            var expectedResult = new List<Product>()
              {
                new Product()
                {
                  Id = 0,
                  Title = "title",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type",
                  Image = "image"
                },
                new Product()
                {
                  Id = 1,
                  Title = "title1",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type1",
                  Image = "image1"
                },
              };

            mockProductService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockProductService.Object.GetByType("type");

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public void IfDeleteProduct()
        {
            //Arrange
            var mockProductService = new Mock<IProductService>();
            var expectedResult = new List<Product>()
              {
                new Product()
                {
                  Id = 0,
                  Title = "title",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type",
                  Image = "image"
                },
                new Product()
                {
                  Id = 1,
                  Title = "title1",
                  Description = "desc",
                  Price = 15,
                  Weight = 1,
                  Type ="type1",
                  Image = "image1"
                },
              };

            mockProductService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            mockProductService.Object.Delete(0);
            var actualResult = mockProductService.Object.GetAll();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
        }
    }
}
