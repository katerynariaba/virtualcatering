﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualCatering.Api.Services.Abstract;
using VirtualCatering.Domain.DomainModels;
using Xunit;

namespace VirtualCatering.UnitTests
{
    public class UserUnitTests
    {
        [Fact]
        public void IfGetAllReturnCorrectQuantity()
        {
            //Arrange
            var mockUserService = new Mock<IUserService>();
            var expectedResult = new List<User>()
              {
                new User()
                {
                  Id = 0,
                  Login = "login",
                  Role = "table",
                  Description ="desc"
                },
                new User()
                {
                  Id = 1,
                  Login = "login1",
                  Role = "table",
                  Description ="desc"
                },
              };

            mockUserService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockUserService.Object.GetAll();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
        }

        [Fact]
        public void IfGetAllAllItemsAreNotNull()
        {
            //Arrange
            var mockUserService = new Mock<IUserService>();
            var expectedResult = new List<User>()
              {
                new User()
                {
                  Id = 0,
                  Login = "login",
                  Role = "table",
                  Description ="desc"
                },
                new User()
                {
                  Id = 1,
                  Login = "login1",
                  Role = "table",
                  Description ="desc"
                },
              };

            mockUserService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockUserService.Object.GetAll();

            //Assert
            Assert.NotNull(actualResult);
        }

        [Fact]
        public void IfGetByIdReturnNotNull()
        {
            //Arrange
            var mockUserService = new Mock<IUserService>();
            var expectedResult = new List<User>()
              {
                new User()
                {
                  Id = 0,
                  Login = "login",
                  Role = "table",
                  Description ="desc"
                },
                new User()
                {
                  Id = 1,
                  Login = "login1",
                  Role = "table",
                  Description ="desc"
                },
              };

            mockUserService.Setup(r => r.GetAll()).Returns(expectedResult);

            //Act
            var actualResult = mockUserService.Object.GetById(0);

            //Assert
            Assert.Null(actualResult);
        }
    }
}
